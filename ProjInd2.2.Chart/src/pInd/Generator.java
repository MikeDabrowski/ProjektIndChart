package pInd;

import pInd.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Generator {
	private double x,y;
	
	
	
	
	public ArrayList<Point> generateXY(double xmin, double xmax,double ymin,double ymax, int n){
		ArrayList<Point> randomPoints = new ArrayList<Point>();
		for(int j=0;j<n;j++){
			x=ThreadLocalRandom.current().nextDouble(xmin-5, xmax+5);
			y=ThreadLocalRandom.current().nextDouble(ymin-5, ymax+5);
			x*=1000;
			y*=1000;
			x=Math.round(x);
	        x/=1000;
	        y=Math.round(y);
	        y/=1000;
			randomPoints.add(new Point(x,y));
		}
		return randomPoints;
	}
	
	public void show(ArrayList<Point> randomPoints){
		System.out.println("\tx\ty");
		for(int i=0;i<randomPoints.size();i++){
			System.out.println(i+".\t"+((randomPoints.get(i))).getXX()+"\t"+((randomPoints.get(i))).getYY());
			if(i%5000==0&&i!=0){
				try {
					System.in.read();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println( );
	}
	
}
