package pInd;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import org.jfree.ui.RefineryUtilities; 

import pInd.MyChart;
import pInd.Generator;
import pInd.Point;
//import java.awt.Point;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		//rev eng algo
		// how to get rid of point inside hull
		
		File file1 = new File("src/pInd/input1.txt");
		File file2 = new File("src/pInd/input2.txt");
		File file3 = new File("src/pInd/input3.txt");
		File file4 = new File("src/pInd/input4.txt");
		
		int randomPointsNum=1000;//specify how many random points will be used
		boolean showSteps=true;//if you want to see input points, points of convexhull...
		boolean withPlot=true;//if you want plot
		boolean savePlot=false;//if you want to save plot
		
	/*	System.out.println("find area using "+randomPointsNum+"numbers = " + 
				findArea(readFromFile(file2),1000,showSteps,true,true));*/
		double avg=0;
		double area=0;
		//show area
//		for(int j=10;j<=100;j+=10){
			avg=0;
			for(int i=0;i<1;i++){
				area=findArea(readFromFile(file4),randomPointsNum,showSteps,withPlot,savePlot);
//				System.out.println("find area using "+randomPointsNum+"numbers = " +area);
				avg+=area;
			}
			//System.out.println("avg= \t"+avg / 10);
//		}
		
		

	}
	
	public static double findArea(ArrayList<Point> inputPoints, int randomPointsNum, boolean showSteps, boolean withPlot, boolean savePlot){
		Generator draw=new Generator(); 
		ArrayList<Point> inputPoints2 = new ArrayList<Point>(inputPoints);//stores initial points unchanged 
		
		ArrayList<Point> convexHull = new ArrayList<Point>();//points creating hull, shell, whatever
		ArrayList<Point> rect = new ArrayList<Point>();
		double area;
		
		convexHull=quickHull(inputPoints);//find convex hull
		if(showSteps==true){	
			System.out.println("Points creating Convex Hull");
			draw.show(convexHull);
		}
		
		//find and show extremas of convexHull
		if(showSteps==true){
			System.out.print("xmax="+findExtr(convexHull, "E")+"\t");//xmax
			System.out.print("xmin="+findExtr(convexHull, "W")+"\t");//xmin
			System.out.print("ymax="+findExtr(convexHull, "N")+"\t");//ymax
			System.out.println("ymin="+findExtr(convexHull, "S"));//ymin
		}
		
		//generate and show random points between extremas
		if(showSteps==true){
		System.out.println("\nRandom points generated: "+randomPointsNum);}
		rect=draw.generateXY(findExtr(convexHull, "W"), 
						     findExtr(convexHull, "E"), 
						     findExtr(convexHull, "S"), 
						     findExtr(convexHull, "N"), randomPointsNum);
		//draw.show(rect);
		
		//Count how many points is within polygon
		if(showSteps==true){
		System.out.println("How many inside polygon: "+howManyInPoly(rect, convexHull));
		System.out.println(area(convexHull, rect, randomPointsNum));}
		area=area(convexHull, rect, randomPointsNum);
		
		if(withPlot==true){
			plotChart("TiTle", inputPoints2, convexHull, rect, savePlot);
		}
		
		return area;
	}
	
	public static void plotChart(final String title, ArrayList<Point> inputPoints2, ArrayList<Point> convexHull, ArrayList<Point> randomPoints,boolean savePlot){
		final MyChart demo = new MyChart("My Chart", inputPoints2,convexHull,randomPoints,savePlot);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);	
        
	}
	
	public static ArrayList<Point> readFromFile(File file) throws FileNotFoundException{
		//returns coordinates table from points in file
		ArrayList<Point> initialPoints = new ArrayList<Point>();//here points will be stored
		double temp1,temp2;
		Scanner in = new Scanner(file);

	    while(in.hasNextDouble()){
	    	temp1=in.nextDouble();
	    	temp2=in.nextDouble();
	    	initialPoints.add(new Point(temp1, temp2));
	    }
	    in.close();
		return initialPoints;
	}
	
	public static ArrayList<Point> quickHull(ArrayList<Point> points) {
	    //as input we have all points from wich we want to find convexhull
		ArrayList<Point> convexHull = new ArrayList<Point>();//output arraylist
	    if (points.size() < 3) return (ArrayList)points.clone();//check if we need to find convexhull because 3 points are already a convexhull
	    // find extremals - initial points on X axis
	    int minPoint = -1, maxPoint = -1;
	    double minX = java.lang.Double.MAX_VALUE;
	    double maxX = java.lang.Double.MIN_VALUE;
	    for (int i = 0; i < points.size(); i++) {
	      if ((points.get(i)).getXX() < minX) {
	        minX = (points.get(i)).getXX();
	        minPoint = i;
	      } 
	      if ((points.get(i)).getXX() > maxX) {
	        maxX = (points.get(i)).getXX();
	        maxPoint = i;       
	      }
	    }
	    //add those two points to output set and remove them from 'points' set which holds points still to be checked
	    Point A = points.get(minPoint);
	    Point B = points.get(maxPoint);
	    convexHull.add(A);
	    convexHull.add(B);
	    points.remove(A);
	    points.remove(B);
	    
	    //define subsets above and below line of the initial points
	    ArrayList<Point> leftSet = new ArrayList<Point>();
	    ArrayList<Point> rightSet = new ArrayList<Point>();
	    
	    
	    for (int i = 0; i < points.size(); i++) {//for the remaining points
	      Point p = points.get(i);//get point
	      if (pointLocation(A,B,p) == -1)//check on which side falls this point and add it to appropriate subset
	        leftSet.add(p);
	      else
	        rightSet.add(p);
	    }
	    //and here is where shit gets real
	    hullSet(A,B,rightSet,convexHull);//for one side of AB
	    hullSet(B,A,leftSet,convexHull);//other side of AB, switching to BA  changes signs thus method distance looks 'below' 
	    //for farthest point
	    	    
	    return convexHull;
	  }
		
	public static double distance(Point A, Point B, Point C) {
		//find not the magnitude but just the furthest distance
	    double ABx = (B).getXX()-(A).getXX();
	    double ABy = (B).getYY()-(A).getYY();
	    double num = ABx*((A).getYY()-(C).getYY())-ABy*((A).getXX()-(C).getXX());
	    if (num < 0) num = -num;
	    return num;
	}
	  
	public static void hullSet(Point A, Point B, ArrayList<Point> set, ArrayList<Point> hull) {
		//at the beginning point a and b are our initial extremal points
		//set is the set where are points to check
		//hull is the output set containing hull points
		int insertPosition = hull.indexOf(B);
		//exiting statements!!!
	    if (set.size() == 0) return;//if there are no more points
	    if (set.size() == 1) {//if one point left
	      Point p = set.get(0);//get this point
	      set.remove(p);//remove from remaining points
	      hull.add(insertPosition,p);//add to output
	      return;//exit this method
	    }
	    double dist = Double.MIN_VALUE;//set dist to be the smallest double
	    int furthestPoint = -1;//flag ?
	    for (int i = 0; i < set.size(); i++) {//find the furthest point
	      Point p = set.get(i);//get point
	      double distance  = distance(A,B,p);//check distance
	      if (distance > dist) {//compare distance with previous max distance
	        dist = distance;
	        furthestPoint = i;
	      }
	    }
	    //now remove farthest point from set of remaining points and add it to hull
	    Point P = set.get(furthestPoint); 
	    set.remove(furthestPoint);
	    hull.add(insertPosition,P);
	    
	    //now we got new lines AP and PB, we still are on the same side of AB
	    // Determine who's to the left of AP
		ArrayList<Point> leftSetAP = new ArrayList<Point>();
		for (int i = 0; i < set.size(); i++) {
			Point M = set.get(i);
			if (pointLocation(A,P,M)==1) {
				leftSetAP.add(M);
			}
		}
	
		// Determine who's to the left of PB
	    ArrayList<Point> leftSetPB = new ArrayList<Point>();
	    for (int i = 0; i < set.size(); i++) {
	    	Point M = set.get(i);
	    	if (pointLocation(P,B,M)==1) {
	    		leftSetPB.add(M);
	    	}
	    }
	    //call this method for new data
	    hullSet(A,P,leftSetAP,hull);
	    hullSet(P,B,leftSetPB,hull);
	  }
	
	public static double pointLocation(Point A, Point B, Point P) {
	    double cp1 = ((B).getXX()-(A).getXX())*((P).getYY()-(A).getYY()) - ((B).getYY()-(A).getYY())*((P).getXX()-(A).getXX());
	    return (cp1>0)?1:-1;
	    //to know on which side of AB a third point P falls, need to compute cross product AB x AP and check its sign. (vectors)
	}
	
	public static int findExtr(ArrayList<Point> convexHull, String whichOne){
		if(whichOne=="N"){
			//biggest y
			int y=0;
			for(int i=0;i<convexHull.size();i++){
				if(y<=(int)(convexHull.get(i)).getYY()){
					y=(int)(convexHull.get(i)).getYY();
				}				
			}
			return y;
		}
		else if(whichOne=="E"){
			//biggest x
			int x=0;
			for(int i=0;i<convexHull.size();i++){
				if(x<=(int)(convexHull.get(i)).getXX()){
					x=(int)(convexHull.get(i)).getXX();
				}				
			}
			return x;
		}else if(whichOne=="W"){
			//smallest x
			int x=(int)(convexHull.get(1)).getX();;
			for(int i=0;i<convexHull.size();i++){
				if(x>=(int)(convexHull.get(i)).getXX()){
					x=(int)(convexHull.get(i)).getXX();
				}				
			}
			return x;
		}else if(whichOne=="S"){
			//smallest y
			int y=(int)(convexHull.get(1)).getY();
			for(int i=0;i<convexHull.size();i++){
				if(y>=(int)(convexHull.get(i)).getYY()){
					y=(int)(convexHull.get(i)).getYY();
				}				
			}
			return y;
		}else{
			return 0;
		}
	}
	
	public static boolean pointInPoly(ArrayList<Point> convexHull,double x, double y){
		//VERY USEFUL STUFF TO CHECK POINT IN POLYGON!!!
		//how many times arbitrary line crosses polygon border
		int i, j;
		boolean c = false;
		//(convexHull.get(i)).getX()
		  for (i = 0, j = convexHull.size()-1; i < convexHull.size(); j = i++) {
		    if ( (((convexHull.get(i)).getYY()>y) != ((convexHull.get(j)).getYY()>y)) &&
			 (x < ((convexHull.get(j)).getXX()-(convexHull.get(i)).getXX()) * (y-(convexHull.get(i)).getYY()) /
					 ((convexHull.get(j)).getYY()-(convexHull.get(i)).getYY()) + (convexHull.get(i)).getXX()) )
		       c = !c;
		  }
		  return c;
	}
	
	public static int howManyInPoly(ArrayList<Point> pointsToCheck,ArrayList<Point> convexHull){
		int result=0;
		for(int i=0;i<pointsToCheck.size();i++){
			if(pointInPoly(convexHull, ((pointsToCheck.get(i))).getXX(), ((pointsToCheck.get(i))).getYY())==true){
				result++;
			}
		}
		return result;
	}
	
	public static double area(ArrayList<Point> convexHull,ArrayList<Point> rect, int total){
		double area,inside=howManyInPoly(rect, convexHull);
		double rectArea=(findExtr(convexHull, "E")-findExtr(convexHull, "W")+10)*(10+findExtr(convexHull, "N")-findExtr(convexHull, "S"));
		area=inside/total;
		area*=rectArea;
		
		return area;
	}
}