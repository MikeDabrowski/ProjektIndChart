package pInd;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.jfree.chart.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.*;
import org.jfree.ui.ApplicationFrame;
import org.jfree.util.ShapeUtilities;

public class MyChart extends ApplicationFrame{

	public MyChart(final String title, ArrayList<Point> inputPoints, ArrayList<Point> convexHull, ArrayList<Point> randomPoints,boolean savePlot){
		super(title);
		XYDataset dataset1 = createDataset(inputPoints,convexHull,randomPoints);
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "X",
	            "Y",
	            dataset1,
	            PlotOrientation.VERTICAL,
	            true,
	            false,	
	            false
	        );
		XYPlot plot = (XYPlot) chart.getPlot();
		NumberAxis domainAxis =(NumberAxis) plot.getDomainAxis();
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();		
		double xmax=Main.findExtr(convexHull, "E")+6;//xmax
		double xmin=Main.findExtr(convexHull, "W")-6;//xmin
		double ymax=Main.findExtr(convexHull, "N")+6;//ymax
		double ymin=Main.findExtr(convexHull, "S")-6;//ymin
		domainAxis.setRange(xmin, xmax);
		domainAxis.setTickUnit(new NumberTickUnit(1));
		rangeAxis.setRange(ymin, ymax);
		rangeAxis.setTickUnit(new NumberTickUnit(1));
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(); 
        //InputPointsSettings
        renderer.setSeriesLinesVisible(0, false);//inputpoints
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesShape(0, ShapeUtilities.createDownTriangle(2));
        //ConvexHullSettings1
        renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(1, true); 
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setSeriesShape(1, ShapeUtilities.createRegularCross(4, 4));
        //ConvexHullSettings2
        renderer.setSeriesLinesVisible(2, false);
        renderer.setSeriesShapesVisible(2, false);
        renderer.setSeriesPaint(2, Color.RED);
        renderer.setSeriesShape(2, ShapeUtilities.createRegularCross(2, 2));
        
        //RandomPointsSettings
        renderer.setSeriesLinesVisible(3, false);
        renderer.setSeriesShapesVisible(3, true);
        renderer.setSeriesPaint(3, Color.BLACK);
        renderer.setSeriesShape(3, new Ellipse2D.Double(1,1,1,1));
        
        //RectangleSettings1
        renderer.setSeriesLinesVisible(4, false);//rect
        renderer.setSeriesShapesVisible(4, false);
        renderer.setSeriesPaint(4, Color.GREEN);
        renderer.setSeriesShape(4, ShapeUtilities.createUpTriangle(3));
        //RectangleSettings2
        renderer.setSeriesLinesVisible(5, false);//rect
        renderer.setSeriesShapesVisible(5, false);
        renderer.setSeriesPaint(5, Color.GREEN);
        renderer.setSeriesShape(5, ShapeUtilities.createUpTriangle(3));
        
        plot.setRenderer(renderer);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(1000, 1000));
        setContentPane(chartPanel);
        if(savePlot==true){
		    SimpleDateFormat sdfDate = new SimpleDateFormat("HH-mm-ss-SSS");//dd/MM/yyyy
		    Date now = new Date();
		    String strDate = sdfDate.format(now);
		    try{
		    	final ChartRenderingInfo info=new ChartRenderingInfo(new StandardEntityCollection());
		    	final File filre=new File("Chart_"+randomPoints.size()+"_"+strDate+".jpeg");
		    	ChartUtilities.saveChartAsJPEG(filre, chart, 800, 800);
		    }
		    catch(Exception e){}
        }
	}
	
	private XYDataset createDataset(ArrayList<Point> inputPoints, ArrayList<Point> convexHull, ArrayList<Point> randomPoints) {
        XYSeries series0 = new XYSeries("Input points");
        for(int i=0;i<inputPoints.size();i++){
        	series0.add((inputPoints.get(i)).getXX(), (inputPoints.get(i)).getYY());
        }
        XYSeries series1 = new XYSeries("ConvexHull");
        for(int i=0;i<convexHull.size();i++){
        	series1.add((convexHull.get(i)).getXX(), (convexHull.get(i)).getYY());
        }
        XYSeries series2 = new XYSeries("ConvexHull2");
        series2.add((convexHull.get(0)).getXX(),(convexHull.get(0)).getYY());
        series2.add((convexHull.get(convexHull.size()-1)).getXX(),(convexHull.get(convexHull.size()-1)).getYY());
        
        XYSeries series3 = new XYSeries("RandomPoints");
        for(int i=0;i<randomPoints.size();i++){
        	series3.add((randomPoints.get(i)).getXX(), (randomPoints.get(i)).getYY());
        }
        XYSeries series4 = new XYSeries("Rectangle");
        series4.add(Main.findExtr(convexHull, "W"),Main.findExtr(convexHull, "S"));
        series4.add(Main.findExtr(convexHull, "W"),Main.findExtr(convexHull, "N"));
        series4.add(Main.findExtr(convexHull, "E"),Main.findExtr(convexHull, "N"));
        series4.add(Main.findExtr(convexHull, "E"),Main.findExtr(convexHull, "S"));
        XYSeries series5 = new XYSeries("Rectangle2");
        series5.add(Main.findExtr(convexHull, "W"),Main.findExtr(convexHull, "S"));
        series5.add(Main.findExtr(convexHull, "E"),Main.findExtr(convexHull, "S"));
        
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series0);
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);
        dataset.addSeries(series4);
        dataset.addSeries(series5);
        return dataset;
    }
}
