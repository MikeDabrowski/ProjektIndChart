package pInd;

import java.util.Comparator;

public class Point implements Comparator<Point>{
	public int x,y;
    public double xx,yy;
    public int getX() { return x;}
    public int getY() { return y;}
    public double getXX() { return xx;}
    public double getYY() { return yy;}
    public void setX(int x) { this.x=x;}
    public void setY(int y) { this.y=y;}
    public void setXX(double xx) { this.xx=xx;}
    public void setYY(double yy) { this.yy=yy;}

	public Point(int x,int y) { this.x=x; this.y=y;}
	public Point(double xx,double yy) { this.xx=xx; this.yy=yy;}
	@Override
	public int compare(Point arg0, Point arg1) {
		if (arg0.xx < arg1.xx) {
            return -1;
        }
        else if (arg1.xx > arg1.xx) {
            return 1;
        }
        else {
            return 0;
        }
	}
}
